# plombot-js

Discord Music Bot in JS with DotA features.

[Invite **plombot** to a server](https://discordapp.com/oauth2/authorize?scope=bot&permissions=1110453312&client_id=635250960221863947)

## Running

1. Go to [Discord's Developer Portal](https://discordapp.com/developers/applications/) and create an application.

1. Select your app and on the Bot tab, create a Bot for the application.

1. Create a file named `.env` with the following format using your bot's secret key:
    ```
    BOT_TOKEN=NjM1MjUwOTYwMgIxODYcOTQ3.XakFIg.N5BaVYZQYiJywMuP7JSqGbLUmG1
    ```

1. Run the bot
    ```bash
    # Auto-restart on file change
    npm run dev

    # Normal usage
    npm run start
    ```

1. Invite the bot to your server (replace with your client ID)
    ```
    https://discordapp.com/oauth2/authorize?scope=bot&permissions=1110453312&client_id=635250960221863947
    ```