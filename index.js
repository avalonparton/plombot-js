require('dotenv').config()
const Discord = require('discord.js')
const client = new Discord.Client()

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`)
})

client.on('message', message => {

  // Ignore PMs
  if (!message.guild) return;

  if (message.content === '/join') {
    // Make sure user is connected to a voice channel
    if (message.member.voiceChannel) {
      message.member.voiceChannel.join()
        .then(connection => {
          message.reply('I am here!');
        })
        .catch(console.log);
    } else {
      message.reply('Connect to a voice channel first!');
    }
  }
});

client.login(process.env.BOT_TOKEN)